import java.util.Random;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		var random = new Random();
		var listener = new Listener();
		var objectOne = new ObjectOne();
		var objectTwo = new ObjectTwo();
		var objectThree = new ObjectThree();
		var objectFour = new ObjectFour();
		
		while (true){
			int value = random.nextInt(1, 5);
			System.out.println(value);
			switch(value) {
				case 1:
					listener.AddListeners(objectOne);
					break;
				case 2:
					listener.AddListeners(objectTwo);
					break;
				case 3:
					listener.AddListeners(objectThree);
					break;
				case 4:
					listener.AddListeners(objectFour);
					break;
			}
			Thread.sleep(1000);
			listener.Response();
			listener.ClearListeners();
		}
	}
}
