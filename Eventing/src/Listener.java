import java.util.ArrayList;
import java.util.List;

public class Listener {
	
	private List<IListener> listeners = new ArrayList<IListener>();
	
	public void AddListeners(IListener listener){
		listeners.add(listener);
	}
	
	public void ClearListeners(){
		listeners.clear();
	}
	
	public void Response(){
		for(IListener listener : listeners) {
			listener.Response();
		}
	}
}
