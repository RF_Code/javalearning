import java.util.Scanner;

public class FirstLesson {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Podaj wartość: ");
		// Next() Method get only first word, Method NextLine() get all line
		String name = scanner.nextLine();
		System.out.print("Hello " + name);
		System.out.println("Hello " + name);
		System.out.printf("Hello %s", name);
		scanner.close();
	}
}
