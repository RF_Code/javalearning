
public class Dacia implements ICar {

	private String name = "Dacia";
	private Double price;
	
	public Dacia(Double price) {
		this.price = price;
	}
	
	@Override
	public void getInformation() {
		System.out.println("Auto name: " + this.name);
		System.out.println("Auto price: " + this.price);
	}
}
