import java.util.ArrayList;
import java.util.List;

public class CarFactory {

	private static List<ICar> cars = new ArrayList<ICar>();
	
	public CarFactory() {
		cars.add(new Audi(120000.0));
		cars.add(new BMW(145000.0));
		cars.add(new Dacia(90000.0));
		cars.add(new Mercedes(220000.0));
	}
	
	public void getCars() {
		cars.forEach(car -> car.getInformation());
	}
}
