
public class Audi implements ICar {

	private String name = "Audi";
	private Double price;
	
	public Audi(Double price) {
		this.price = price;
	}
	
	@Override
	public void getInformation() {
		System.out.println("Auto name: " + this.name);
		System.out.println("Auto price: " + this.price);
	}
}
