
public class BMW implements ICar {

	private String name = "BMW";
	private Double price;
	
	public BMW(Double price) {
		this.price = price;
	}
	
	@Override
	public void getInformation() {
		System.out.println("Auto name: " + this.name);
		System.out.println("Auto price: " + this.price);
	}
}
