import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		CarFactory carFactory = new CarFactory();
		carFactory.getCars();
		System.out.println("_______________________");
		Scanner scan = new Scanner(System.in);
		System.out.println("Please selecy value: 1- Audi; 2- BMW; 3- Dacia; 4- Mercedes: ");
		String selectValue = scan.nextLine();
		
		ICar context = null;
		
		switch(selectValue){
		case "1":
			context = new Audi(120000.0);
			break;
		case "2": 
			context = new BMW(145000.0);
			break;
		case "3":
			context = new Dacia(90000.0);
			break;
		case "4":
			context = new Mercedes(220000.0);
			break;
		}
		
		if (context != null)
		{
			context.getInformation();
		}
		else {
			System.out.println("You choose wrong");
		}
		
	}

}
