
public class Mercedes implements ICar {

	private String name = "Mercedes";
	private Double price;
	
	public Mercedes(Double price) {
		this.price = price;
	}
	
	@Override
	public void getInformation() {
		System.out.println("Auto name: " + this.name);
		System.out.println("Auto price: " + this.price);
	}
}
